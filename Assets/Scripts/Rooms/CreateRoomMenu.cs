﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class CreateRoomMenu : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private TextMeshProUGUI _roomName;


    private RoomCanvases _roomCanvases;

    public void FirstInitialize(RoomCanvases canvases)
    {
        _roomCanvases = canvases;
    }

    public void OnClick_CreateRoom()
    {
        if (!PhotonNetwork.IsConnected)
        {
            return;
        }
        RoomOptions options = new RoomOptions();
        options.PublishUserId = true;
        options.MaxPlayers = 12;
        if (_roomName.text == "")
        {

            PhotonNetwork.JoinOrCreateRoom("Default Room", options, TypedLobby.Default);
        }
        else
        {
            PhotonNetwork.JoinOrCreateRoom(_roomName.text, options, TypedLobby.Default);
        }

    }

    

    public override void OnCreatedRoom()
    {
        
        Debug.Log("Created room succesfully.", this);
        _roomCanvases.CurrentRoomCanvas.Show();
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room creation failed", this);
    }
}
