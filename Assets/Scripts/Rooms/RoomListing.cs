﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class RoomListing : MonoBehaviour
{
    
    [SerializeField]
    private TextMeshProUGUI _text;
    [SerializeField]
    private CreateRoomMenu _createRoomMenu;
    
    public RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        _text.text = (roomInfo.PlayerCount-1)+"/"+(roomInfo.MaxPlayers-1)+ ", " + roomInfo.Name;
    }

    public void OnClick_Button()
    {
        PhotonNetwork.JoinRoom(RoomInfo.Name);
    }
}
