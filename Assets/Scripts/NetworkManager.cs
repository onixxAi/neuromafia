﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviourPunCallbacks
{

    public GameObject PlayerPrefab;
    public GameObject FreePlacePrefab;
    public GameObject CardsPrefab;
    // Start is called before the first frame update
    void Start()
    {
        CreatePlaces();
        CreatePlayers();
        

    }

    private void CreatePlaces()
    {
        Vector3 Center = transform.position;
        int maxplayers = PhotonNetwork.CountOfPlayers;
        Debug.Log(maxplayers);
        int currentAng = 0;
        for (int i = 0; i < PhotonNetwork.CurrentRoom.MaxPlayers; i++)
        {
            Vector3 pos = RandomUtilites.RandomCircle(Center, 8.0f, currentAng);
            Quaternion rot = Quaternion.FromToRotation(Vector3.forward, Center - pos);
            //GameObject card = PhotonNetwork.Instantiate(CardsPrefab.name, new Vector3(0, 0, 0), Quaternion.identity);
            //card.SendMessage("InstantiateCard", arr);
            GameObject _freePlace = Instantiate(FreePlacePrefab, pos, Quaternion.identity);
            _freePlace.SendMessage("ChangeIndex", i);
            currentAng += 360 / PhotonNetwork.CurrentRoom.MaxPlayers;
        }
    }

    public void HandOutCards()
    {
        int mafiaCard = PhotonNetwork.CurrentRoom.PlayerCount / 4;
        int doctorCard = 1;
        int policeCard = 1;
        int nightButterflyCard = 0;
        int townsmanCard = PhotonNetwork.CurrentRoom.PlayerCount - mafiaCard - doctorCard - policeCard - nightButterflyCard;
        List<string> cards = new List<string>() { };
        // if (nightButterflyCard > 0) cards.Add(nightButterflyCard);
        for (int i = 0; i < PhotonNetwork.CurrentRoom.PlayerCount-1; i++)
        {
            if (townsmanCard > 0)
            {
                cards.Add("Townsman");
                townsmanCard -= 1;
            }
            if (mafiaCard > 0)
            {
                cards.Add("Mafia");
                mafiaCard -= 1;
            }
            if (doctorCard > 0)
            {
                cards.Add("Doctor");
                doctorCard -= 1;
            }
            if (policeCard > 0)
            {
                cards.Add("Police");
                policeCard -= 1;
            }


        }
        print("cards:" + cards.Count);
        List<string> randomList = new List<string>();

        System.Random r = new System.Random();
        int randomIndex = 0;
        while (cards.Count > 0)
        {
            randomIndex = r.Next(0, cards.Count); //Choose a random object in the list
            randomList.Add(cards[randomIndex]); //add it to the new, random list
            cards.RemoveAt(randomIndex); //remove to avoid duplicates
        }
        for (int j = 0; j < PhotonNetwork.CurrentRoom.MaxPlayers; j++)
        {
            string arr = randomList[j] + "," + j;
            GameObject card = PhotonNetwork.Instantiate(CardsPrefab.name, new Vector3(0, 0, 5), Quaternion.identity);
            card.SendMessage("InstantiateCard", arr);

        }



        IEnumerator Timer(int time)
        {
            yield return new WaitForSeconds(time);

        }

        
    }
    void CreatePlayers()
    {
        Vector3 randomPos = new Vector3(0,0,0);
        PhotonNetwork.Instantiate(PlayerPrefab.name, randomPos, Quaternion.identity);
    }
}
