﻿
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreePlace : MonoBehaviour, IPunObservable
{
    public int index=0;
    public bool isFree = true;
    [SerializeField]
    private Collider childCol;
    [SerializeField]
    private GameObject avatar;
    public PhotonView photonView;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (transform.position==other.transform.position )
        {
            avatar.SetActive(false);
            isFree = false;
        }
    }
    public void ChangeIndex(int newIndex)
    {
        index = newIndex;
    }
    private void OnTriggerExit(Collider other)
    {
        avatar.SetActive(true);
        isFree = true;

    }
    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("YEY");
    }
    [PunRPC]
    public void Busy()
    {
        isFree = false;
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(isFree);
            stream.SendNext(index);
        }

        else
        {
            isFree = (bool)stream.ReceiveNext();
            index = (int)stream.ReceiveNext();
        }
    }
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    photonView.RPC("Busy", RpcTarget.Others);
        //}
    }
}