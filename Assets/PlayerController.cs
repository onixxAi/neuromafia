﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour, IPunObservable
{
    PhotonView photonView;
    public TextMeshPro _nickname;
    public GameObject _avatar;
    public Sprite programmer;
    private bool isRed;
    private bool isMaster;

    public string role;

    public bool isVoting;
    public bool isArrest;
    public bool isDead;
    public bool isHealthy;
    public bool isFucked;


    private void Start()
    {
        photonView = GetComponent<PhotonView>();
        if (photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                isRed = true;
            }
		//PhotonNetwork.NickName.Normalize()== "Programmer".Normalize()
            //print(string.Compare(PhotonNetwork.NickName.Normalize(), "Programmer".Normalize())==0);
            if (string.Compare(PhotonNetwork.NickName.Normalize(), "Programmer".Normalize())==0);
            {
                PhotonNetwork.NickName = "Я программист, а не математик";
                //_avatar.GetComponent<SpriteRenderer>().sprite = programmer;

            }
            if (PhotonNetwork.IsMasterClient)
            {
                //_avatar.SetActive(false);
                _nickname.text ="";
                role = "Master";
                transform.position = new Vector3(15, 8, 0);
                isMaster = true;


            }
            else
            {
                GameObject.Find("MasterWindowCanvas").SetActive(false);
            }
            _nickname.text = PhotonNetwork.NickName;
        }
        GetComponent<TouchTransform>().enabled = !isMaster;
    }
    private void Update()
    {
        if (photonView.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                isRed = !isRed;
            }
        }
        if (isRed)
        {
            _avatar.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else
        {
            _avatar.GetComponent<SpriteRenderer>().color = Color.white;
        }
        RoleTouch();
    }

    private void RoleTouch()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.gameObject.name);
                if (hit.transform.CompareTag("Player"))
                {
                    Debug.Log("Its Player");
                    var target = hit.transform.GetComponent<PlayerController>();
                    if (isVoting)
                    {
                        //проведение голосования
                    }
                    else
                    {
                        if (role.Normalize() == "Police".Normalize())
                        {
                            //арест
                            target.isArrest = true;
                        }
                        if (role.Normalize() == "Mafia".Normalize())
                        {
                            //убийство
                            target.isDead = true;
                        }
                        if (role.Normalize() == "Doctor".Normalize())
                        {
                            //лечение
                            target.isHealthy = true;
                        }
                        if (role.Normalize() == "NightButterfly".Normalize())
                        {
                            //трахание
                            target.isFucked = true;
                        }
                    }
                }
            }
        }
    }

    void OnMouseDown()
    {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(isRed);
            stream.SendNext(isMaster);
            stream.SendNext(_nickname.text);
            stream.SendNext(role);
            //stream.SendNext(_avatar);
        }
        else
        {
            isRed = (bool)stream.ReceiveNext();
            isMaster = (bool)stream.ReceiveNext();
            _nickname.text = (string)stream.ReceiveNext();
            role = (string)stream.ReceiveNext();
            //_avatar = (GameObject)stream.ReceiveNext();
        }
    }
}
