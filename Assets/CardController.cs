﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour, IPunObservable
{
    public string role;
    public int index;
    public string lol;
    public PhotonView photonView;
    Vector2 targetPos;
    public void InstantiateCard(string arr)
    {
        lol = arr;
        string[] words = lol.Split(',');
        print(words[1]);
        role = Convert.ToString(words[0]);
        index = Int32.Parse(words[1]);

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(role);
            stream.SendNext(index);
            //stream.SendNext(transform.position);
        }
        else
        {
            //transform.position = (Vector3)stream.ReceiveNext();
            role = (string)stream.ReceiveNext();
            index = (int)stream.ReceiveNext();
        }
    }
    
    private void Start()
    {
        var player = GameObject.Find("Player(Clone)" + index);
        photonView = PhotonView.Get(this);
        targetPos = player.transform.position;
        photonView.RPC("ChangeRole", RpcTarget.All, player.GetComponent<PlayerController>().role);
        player.GetComponent<PlayerController>().role = role;
    }
    [PunRPC]
    public void ChangeRole(string playerRole)
    {
        playerRole = role;
    }

    private void Update()
    {
        if ((Vector2)transform.position != targetPos&&PhotonNetwork.IsMasterClient)
            transform.position = Vector2.MoveTowards(transform.position, targetPos, 10 * Time.deltaTime);
    }
}
