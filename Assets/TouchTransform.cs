﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTransform : MonoBehaviour, IPunObservable
{
    string oldName;
    public int placeIndex=0;
    PhotonView photonView;
    Collider2D col;
    Vector2 targetPos;
    public bool isMovement = false;
    // Start is called before the first frame update
    void Start()
    {
        oldName = gameObject.name;
        col = GetComponent<Collider2D>();
        photonView = GetComponent<PhotonView>();
        targetPos = transform.position;
        gameObject.name = gameObject.name + placeIndex;
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.IsMine)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (isMovement == false)
            {
                if (Input.GetMouseButton(0))
                {
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.transform.CompareTag("FreePlace"))
                        {
                            if (hit.transform.GetComponent<FreePlace>().isFree)
                            {
                                //if (hit.transform.GetComponent<PhotonView>())
                                //{
                                //    hit.transform.GetComponent<PhotonView>().RPC("Busy", RpcTarget.AllBuffered);
                                //    hit.transform.GetComponent<PhotonView>().
                                //}
                                //hit.transform.GetComponent<FreePlace>().Busy();
                                //hit.transform.gameObject.GetComponent<PhotonView>().RPC("Busy", RpcTarget.Others);
                                placeIndex = hit.transform.GetComponent<FreePlace>().index;
                                photonView.RPC("ChangePlaceIndex",RpcTarget.All, placeIndex);
                                //ChangePlaceIndex(hit.transform.GetComponent<FreePlace>().index);
                                targetPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                                targetPos = hit.transform.position;
                                
                            }

                        }
                    }
                }
            }
           

            if ((Vector2)transform.position != targetPos)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetPos, 100 * Time.deltaTime);
                isMovement = true;
            }
            else
            {
                isMovement = false;

            }

        }

    }
    [PunRPC]
    public void ChangePlaceIndex(int index)
    {
        gameObject.name = oldName;
        gameObject.name = gameObject.name + index;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(placeIndex);
        }
        else
        {
            placeIndex = (int)stream.ReceiveNext();
        }
    }
}
